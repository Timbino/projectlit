<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->integer('id')->unique();            
            $table->text('name');
            $table->text('description');
            $table->integer('effect_1')->nullable();
            $table->integer('effect_2')->nullable();
            $table->integer('effect_3')->nullable();
            $table->integer('exp')->nullable();
            $table->integer('gold')->nullable();
            $table->text('rarity');        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
