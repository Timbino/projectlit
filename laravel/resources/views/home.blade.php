@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    Welcome back {{ Auth::user()->name }}!
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Create a character</div>
                <div class="card-body">                
                <form method="POST" action="/create-character">            
                    @csrf
                    <input type="text" name="name" />
                    <input type="hidden" name="userid" value="{{ Auth::user()->id }}" />
                    <input type="submit" />
                </form>   
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif          
                </div>            
            </div>
            <div class="card">
                <div class="card-header">Available characters</div>
                <div class="card-body">
                @foreach ($characters as $character)
                    <p><a href="/game/{{ $character->id }}">{{ $character->name }} - {{ $character->level }}</a> <a href="/delete-character/{{ $character->id }}" style="color:red;">Delete</a></p>
                @endforeach
                </div>
            </div>            
        </div>
    </div>
</div>
@endsection
