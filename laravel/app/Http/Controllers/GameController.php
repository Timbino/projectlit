<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GameController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id)
    {
        $character = DB::table('characters')->where('id', $id)->first();        
        return view('game', ['character' => $character]);
    }

    public function generateCombat($id){
        
    }
}
