<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharactersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('characters', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->integer('userid');
            $table->text('name');
            $table->integer('strength')->default('5'); 
            $table->integer('intelligence')->default('5');
            $table->integer('stamina')->default('5');
            $table->integer('dexterity')->default('5');
            $table->integer('luck')->default('5');
            $table->integer('gold')->default('100');
            $table->integer('adventures')->default('0');
            $table->integer('level')->default('1');
            $table->integer('experience')->default('0');
            $table->integer('health')->default('100');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characters');
    }
}
