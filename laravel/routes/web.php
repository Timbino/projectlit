<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/game/{id}', 'GameController@index')->name('game');

Route::get('/combat/{id}', 'GameController@generateCombat')->name('game');

Route::post('/create-character', 'CharacterController@createCharacter')->name('home');

Route::get('/delete-character/{id}', 'CharacterController@deleteCharacter')->name('home');