<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->insert([
            'name' => 'tim',
            'email' => 'time_tim@hotmail.com',
            'password' => bcrypt('starcraft123'),
        ]);

        DB::table('users')->insert([            
            'name' => 'Casper',
            'email' => 'caspervugts@hotmail.com',
            'password' => bcrypt('zonnebloem36'),
        ]);

        DB::table('effects')->insert([        
            'id' => '1',
            'description' => 'You are feeling lucky! +2 Luck',
            'stat' => 'luck',
            'value' => '2',
        ]);

        DB::table('effects')->insert([   
            'id' => '2',         
            'description' => 'Headache. -2 stamina.',
            'stat' => 'stamina',
            'value' => '-2',
        ]);

        DB::table('common_events')->insert([
            'id' => '1',
            'name' => 'Treasure!',
            'description' => 'You find a treasure in the woods. It contains some useless items but also 100 gold!',
            'effect_1' => '1',
            'exp' => '15',
            'gold' => '100',
        ]);

        DB::table('common_events')->insert([
            'id' => '2',
            'name' => 'Sucker punch',
            'description' => 'A homeless peasant knocks you out cold. You wake up a few hours later with a soaring headache.',
            'effect_1' => '2',
        ]);

        DB::table('uncommon_events')->insert([
            'id' => '1',
            'name' => 'Money stolen!',
            'description' => 'A thief steals your gold and runs off. You lose 50 gold.',
            'exp' => '10',
            'gold' => '-50',
        ]);
    }
}
