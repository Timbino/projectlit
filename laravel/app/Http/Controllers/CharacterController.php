<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response;
use Contact;
use App\Character;
use Auth;
use DB;

// ▒▒▒░░░░░░░░░░▄▐░░░░
// ▒░░░░░░▄▄▄░░▄██▄░░░
// ░░░░░░▐▀█▀▌░░░░▀█▄░
// ░░░░░░▐█▄█▌░░░░░░▀█▄
// ░░░░░░░▀▄▀░░░▄▄▄▄▄▀▀
// ░░░░░▄▄▄██▀▀▀▀░░░░░
// ░░░░█▀▄▄▄█░▀▀░░░░░░
// ░░░░▌░▄▄▄▐▌▀▀▀░░░░░
// ░▄░▐░░░▄▄░█░▀▀░░░░░
// ░▀█▌░░░▄░▀█▀░▀░░░░░
// ░░░░░░░░▄▄▐▌▄▄░░░░░
// ░░░░░░░░▀███▀█░▄░░░
// ░░░░░░░▐▌▀▄▀▄▀▐▄░░░
// ░░░░░░░▐▀░░░░░░▐▌░░
// ░░░░░░░█░░░░░░░░█░░
// ░░░░░░▐▌░░░░░░░░░█░
// YOU'VE BEEN SPOOKED, ADD 'SORRY MR SKELETON' IN YOUR VOLGENDE COMMIT OR ELSE SPOOKY SKELETON WILL FOLLOW YOU 4 EVER

class CharacterController extends Controller
{
    public function createCharacter(Request $request){                    
        $character = new Character;
        $character->name = $request->name;
        $character->userid = $request->userid;       
        $character->save();

        return redirect('/home')->with('status', 'Character created!');
        // if(!$character->save()){
        //     return view('/home');
        // }else{
        //     return view('/home');
        // }        
    }

    public function deleteCharacter(Request $request){
        DB::table('characters')->where('id', $request->id)->delete();  
        return redirect('/home')->with('status', 'Character deleted!');
    }
}
